import { Mongo } from 'meteor/mongo';

export const Parkings = new Mongo.Collection('parking_collections');

var Schemas = {};
Schemas.info = new SimpleSchema({
  enteranceHeight: {
    type: Number,
    label: "Door Height"
  },
  capacity: {
    type: Number,
    label: "Capacity"
  },
  available: {
    type: Number,
    label: "Available"
  }
});
Schemas.Parkings = new SimpleSchema({
    address: {
        type: String,
        label: "Title"
    },
    owner: {
        type: String,
        label: "Author"
    },
    details: {
      type: Schemas.info
    },
    description: {
      type: String
    },
    createdAt: {
      type: Date
    }
});

Parkings.attachSchema(Schemas.Parkings);
