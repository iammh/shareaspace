'use strict';
import { Meteor } from 'meteor/meteor';
(function () {
  FlowRouter.route('/', {
    triggersEnter: [function (context, redirect) {
      if(Meteor.userId()) {
        redirect('/app');
      }
    }],
    action: function (params , queryParams) {
      BlazeLayout.render('basicApp', {content: 'home'});
    }
  });


  FlowRouter.route('/about', {
    action: function (params , queryParams) {
      BlazeLayout.render('basicApp', {content: 'about'});
    }
  });
  FlowRouter.route('/login', {
    action: function (params , queryParams) {
      BlazeLayout.render('basicApp', {content: 'login'});
    }
  });
  FlowRouter.route('/join', {
    triggersEnter: [
      function (context, redirect) {
        if(Meteor.userId()) {
          redirect('/');
        }
      }
    ],
    action: function (params , queryParams) {
      BlazeLayout.render('basicApp', {content: 'join'});
    }
  });
  FlowRouter.route('/logout', {
    triggersEnter: [
      function (context, redirect) {
        if(!Meteor.userId()) {
          redirect('/');
        }else {
          Meteor.logout();
          redirect('/');
        }
      }
    ],
    action: function (params , queryParams) {
      // BlazeLayout.render('basicApp', {content: 'join'});
    }
  });

})();
