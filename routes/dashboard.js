import { Meteor } from 'meteor/meteor';

(function () {
  function beforeEnter(c, redirect) {
    console.log(Roles.userIsInRole(Meteor.userId(),["admin"]));
    if(!Meteor.userId()) {
      redirect('/');
    }
    else if(Meteor.userId() && !Roles.userIsInRole(Meteor.userId(), ["admin"]) ) {
      redirect('/');
    }
  }
  var Dashboard =  FlowRouter.group({
    prefix: '/dashboard',
    triggersEnter: [beforeEnter]
  });

  Dashboard.route('/', {
    action: function (params, queryParams) {
      BlazeLayout.render('dashboardApp', {content: 'dashboardHome'});
    }
  });


  Dashboard.route('/profile', {
    action: function (params, queryParams) {
      BlazeLayout.render('dashboardApp', {content: 'profile'});
    }
  });
  Dashboard.route('/parkings', {
    triggersEnter: [beforeEnter ],
    subscriptions: function (params) {
      this.register("parkings",Meteor.subscribe("parkings"));
      // this.register("skipFrom",Meteor.subscribe("skipFrom", 1));
    },
    action: function (params, queryParams) {
      BlazeLayout.render('dashboardApp', {content: 'parkings'});
    }
  });
  // Dashboard.route('/parkings/:value', {
  //   triggersEnter: [beforeEnter ],
  //   subscriptions: function (params) {
  //     this.register("skipFrom",Meteor.subscribe("skipFrom", params.value));
  //   },
  //   action: function (params, queryParams) {
  //     BlazeLayout.render('dashboardApp', {content: 'parkings'});
  //   }
  // });
  Dashboard.route('/parkings/add', {
    action: function (params, queryParams) {
      BlazeLayout.render('dashboardApp', {content: 'parking'});
    }
  });
  Dashboard.route('/cars', {
    action: function (params, queryParams) {
      BlazeLayout.render('dashboardApp', {content: 'cars'});
    }
  });
  Dashboard.route('/settings', {
    action: function (params, queryParams) {
      BlazeLayout.render('dashboardApp', {content: 'settings'});
    }
  });

})()
