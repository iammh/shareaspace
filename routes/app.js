'use strict';
import { Meteor } from 'meteor/meteor';

(function () {
  function beforeEnter(context, redirect) {
    if(!Meteor.userId()) {
      FlowRouter.go('/login');
    }
  }
  FlowRouter.route('/app', {
    triggersEnter: [beforeEnter],
    action: function (params, queryParams) {
      BlazeLayout.render('basicApp', {content: 'app'});
    }
  });
  FlowRouter.route('/app/:location', {
    triggersEnter: [beforeEnter],
    subscriptions: function (params) {
      this.register('searchParkings', Meteor.subscribe("parkingsByAddress", params.location))
    },
    action: function (params, queryParams) {
      BlazeLayout.render('basicApp', {content: 'searchResult'});
    }
  });

  FlowRouter.route('/app/parkings/:_id',{
    triggersEnter: [beforeEnter],
    subscriptions: function (params) {
      this.register('parking', Meteor.subscribe("parking", params._id));
      
    },
    action: function (params, queryParams) {
      BlazeLayout.render('basicApp', {content: 'parkingDetails'});
    }
  });
})();
