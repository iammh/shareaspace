import {Parkings} from '../../api/models/parkings.js';
import {check} from 'meteor/check';

Meteor.publish("parkings", function () {
  console.log(this.userId, Roles.userIsInRole(this.userId, ['admin']));
  if(Roles.userIsInRole(this.userId, ['admin'])) {
    return Parkings.find({});
  }
  return Parkings.find({owner: this.userId });
});

Meteor.publish("parking", function (id) {
  check(id, String);
  return Parkings.find({_id: id });
});
Meteor.publish("skipFrom", function (id) {
  check(id, String);
  value = 2 * id;
  return Parkings.find({},{limit: 2, skip: value});
});
Meteor.publish("parkingsByAddress", function (param) {
  check(param, String);
  // console.log(param);
  // console.log(Parkings.find({address: { $regex: param, $options: "i" }  }).count());
  return Parkings.find({address: { $regex: param, $options: "i" } });
})
