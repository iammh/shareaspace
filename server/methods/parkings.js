import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Match } from 'meteor/check';
// import { Roles } from 'meteor/alanning-roles';
import { Accounts } from 'meteor/accounts-base';

import {Parkings} from '../../api/models/parkings.js';

Meteor.methods({
  addParking: function (parking) {
    check(parking, {
      address: String,
      enteranceHeight: String,
      capacity: String,
      description: String
    });

    var park = {
      address: parking.address,
      owner: this.userId,
      details: {
        enteranceHeight: parking.enteranceHeight,
        capacity: parking.capacity,
        available: parking.capacity
      },
      description: parking.description,
      createdAt: new Date()
    };

    Parkings.insert(park);
  },
  addCarToParking: function (parking) {
    check(parking, String);
    var item = Parkings.findOne({_id: parking});
    if(item && item.details.available + 1 <= item.details.capacity) {
      var left = item.details.available + 1;
      console.log(left);
      Parkings.update({_id: parking}, {
        $set: {
          "details.available": left
        }
      });
    } else {
      console.log("No parking found on entry of a car");
    }

  },
  outCarFromParking: function (parking) {
    check(parking, String);
    console.log("GOT A OUT REQUEST OF ",parking);
    var item = Parkings.findOne({_id: parking});
    if( item && item.details.available > 0) {
      var left = item.details.available - 1;
      console.log(left);
      Parkings.update({_id: parking}, {
        $set: {
          "details.available": left
        }
      });
    } else {
      console.log("No parking found on exit of a car");
    }
  },
  test: function (test) {
    check(test, String);
    console.log(test);
  }

});
