import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Match } from 'meteor/check';
// import { Roles } from 'meteor/alanning-roles';
import { Accounts } from 'meteor/accounts-base';

Meteor.methods({
  register: function (user) {
    check(user, {
      email: String,
      password: String,
      profile: {
        name: String
      }
    });
    // check(cb, Match.any);
    var id = Accounts.createUser(user);
    Roles.addUsersToRoles(id, ['normal'])

    if(id) {
      // cb(null);
    }
    else {
      throw new Error("User not created")
    }

  },
  updateName: function (name) {
    check(name, {
      first: String,
      last: String
    });

    try {
      Meteor.users.update(this.userId, {
        $set: {"profile.name": name}
      });
    } catch (e) {

    }
  },
  updateAddress: function (address) {
    check(address, {
      into: String,
      verified: Boolean
    });

    try {
      Meteor.users.update(this.userId, {
        $set: {"profile.address": address}
      });
    } catch (e) {

    }
  }
});
