import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Match } from 'meteor/check';
// import { Roles } from 'meteor/alanning-roles';
import { Accounts } from 'meteor/accounts-base';

Meteor.methods({
  sendParkingInfo: function (rfid) {
    check(rfid, Object)
    // check(cb, Match.any);
    console.log("FROM ANOTHER SERVER ",rfid);
    if(Number(rfid.status) === 1) {
      Meteor.call("outCarFromParking", rfid.parking_id);
    }
    else if(Number(rfid.status) === 0){
      Meteor.call("addCarToParking", rfid.parking_id);
    }else {
      console.log("Unhandled request");
    }


  },
  teststring: function (msg) {
    check(msg, String)
    // check(cb, Match.any);
    console.log(msg);

  }
});
