import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';

Template.join.onRendered(function(){
    var validator = $('#join').validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            }
        },
        submitHandler: function (event) {
          var email = $("#email").val();
          var password = $("#password").val();
          var name = $("#name").val();
          var user = {
            email: email,
            password: password,
            profile: {
              name: name
            }
          };
          Meteor.call("register", user, function (err) {
            if(err) {
              console.error(err);
              validator.showErrors({email: err.reason});
            }
            Meteor.loginWithPassword(email, password, function (err) {
              if(!err) {
                console.log("User loggedin");
              }
              FlowRouter.go('/app');
            });
          });
        }
    });
});



Template.join.events({
  'submit #join'(event, instance) {
    console.log("Join");
    event.preventDefault();

    // $("#join").validate();
  },
});
