import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';

Template.search.onRendered(function () {
  $("#search").focus();
  if(FlowRouter.current().path === '/app')
  {Session.set('token', '');}
});

Template.search.events({
  'keyup #search'(event) {
    // $("#search").val();
    if($("#search").val() == '') {
      Session.set('token', '');
      FlowRouter.go('/app');
    }
    Session.set('token', $("#search").val());
    FlowRouter.go('/app/'+ $("#search").val());
  }
});

Template.search.helpers({
  current(param) {
    if(param == Session.get('token') ) {
      return true;
    }
    else {
      return false;
    }
  },
  value() {
    return Session.get('token');
  }
});
