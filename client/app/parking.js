import {Template} from 'meteor/templating';
import {Meteor} from 'meteor/meteor';
import { Parkings } from '../../api/models/parkings.js';

import "./parkingdetails.html";

Template.parkingDetails.helpers({
  details() {
    // FlowRouter.current().params._id
    // console.log(Parkings.findOne({_id: 'z7HpiniuyZ6uTDkp2'}));
    return Parkings.findOne({_id: FlowRouter.current().params._id});
  },
  checkAvailable(details) {
    if(details.owner === Meteor.user()._id) {
      return true;
    }
    return false;
    
  }
});

Template.parkingDetails.events({
  'click #btnIncrease'() {
    var id = FlowRouter.current().params._id;
    Meteor.call("addCarToParking", id);
  },
  'click #btnDecrease'(event) {
    var id = FlowRouter.current().params._id;
    Meteor.call("outCarFromParking", id);
  }
});
