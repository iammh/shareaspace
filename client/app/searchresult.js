import { Template } from 'meteor/templating';
import { Session } from 'meteor/session';
import {Parkings} from '../../api/models/parkings.js';

// Template.searchResult.onRendered(function () {
//   this.selectedOne = FlowRouter.current().params.location;
// });
Template.searchResult.onRendered(function () {
  $("#search").focus();
});

Template.searchResult.helpers ({
  parkingsCollection() {
    return Parkings.find({});
  },
  found() {
    return Parkings.find({}).count() > 0 ? true: false;
  }
});
