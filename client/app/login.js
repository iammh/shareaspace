import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';

Template.login.onRendered(function(){
    var validator = $('#login').validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            }
        },
        submitHandler: function (event) {
          var email = $("#email").val();
          var password = $("#password").val();

          Meteor.loginWithPassword(email, password, function (err) {
            if(err) {
              console.error(err);
              if(err.reason == "User not found") {
                validator.showErrors({
                  email: "You are not registered yet"
                });
              }else if (err.reason == "Incorrect password"){
                validator.showErrors({
                  password: err.reason
                });
              } else {
                validator.showErrors({
                  password: err.reason
                });
              }

            }else {
              FlowRouter.go('/app');
            }
          });
        }
    });
});

Template.login.helpers({

});

Template.login.events({
  'submit #login'(event, instance) {
    console.log("Login");
    event.preventDefault();
    // var email = $("#email").val();
    // var password = $("#password").val();
    //
    // Meteor.loginWithPassword(email, password, function (err) {
    //   if(err) {
    //     console.error(err);
    //   }
    // });
  },
});
