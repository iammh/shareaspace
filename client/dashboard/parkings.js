import {Template} from 'meteor/templating';
import {Meteor} from 'meteor/meteor';
import {Parkings} from '../../api/models/parkings.js';
Template.parkings.events({
  'click #addParkingLot'(event) {
    BootstrapModalPrompt.prompt({
        title: "Confirm something",
        content: "Do you really want to confirm whatever?"
    }, function(result) {
      if (result) {
        // User confirmed it, so go do something.
      }
      else {
        // User did not confirm, do nothing.
      }
    });
  }
});

Template.parkings.helpers({
  parkingsCollection(){
    return Parkings.find({});
  }
});
