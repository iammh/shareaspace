import {Template} from 'meteor/templating';

Template.parking.onRendered(function(){
    var validator = $('#addParking').validate({
        rules: {
            capacity: {
                required: true,
                number: true
            },
            address: {
                required: true
            }
        },
        submitHandler: function (event) {
          var capacity = $("#capacity").val();
          var enteranceHeight = $("#enteranceHeight").val();
          var address = $("#address").val();
          var description = $("#description").val();
          var parking = {
            capacity: capacity,
            enteranceHeight: enteranceHeight,
            address: address,
            description: description
          };
          console.log(parking);
          Meteor.call('addParking', parking, function (err) {
            if(err) {
              console.log(err);
            }
            $("#capacity").val("");
            $("#enteranceHeight").val("");
            $("#address").val("");
            FlowRouter.go('/dashboard/parkings');
          });
        }
    });
});


Template.parking.events({
  'submit #addParking'(event, instance) {

    event.preventDefault();
    // var email = $("#email").val();
    // var password = $("#password").val();
    //
    // Meteor.loginWithPassword(email, password, function (err) {
    //   if(err) {
    //     console.error(err);
    //   }
    // });
  },
});
