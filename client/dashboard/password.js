import {Template} from 'meteor/templating';
import {Meteor} from 'meteor/meteor';
import {Parkings} from '../../api/models/parkings.js';
Template.password.events({
  'submit #changePassword'(event) {
    event.preventDefault();
  }
});

Template.parkings.helpers({
  parkingsCollection(){
    return Parkings.find({});
  }
});
